package com.chenxin.fastdfs.controller;

import com.chenxin.fastdfs.client.FastDfsClient;
import org.apache.commons.io.IOUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.io.InputStream;

@RestController
public class DownLoadController {

    /**
     * 下载接口
     * @param id
     * @return
     */
    @RequestMapping("/download/{id}")
    public ResponseEntity<byte[]> download(@PathVariable Integer id) {
        InputStream inputStream = FastDfsClient.downFile("group1", "M00/00/00/CgxpDWEKX8GABtP-AAApQ090-AE407.gif");
        ResponseEntity<byte[]> responseEntity = null;
        try {
            byte[] bytes = IOUtils.toByteArray(inputStream);
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setContentType(MediaType.APPLICATION_OCTET_STREAM);
//        httpHeaders.setContentLength(inputStream);
            httpHeaders.setContentDispositionFormData("attachment","nicaicai");
            responseEntity = new ResponseEntity<>(bytes,httpHeaders,HttpStatus.OK);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return responseEntity;
    }
}
