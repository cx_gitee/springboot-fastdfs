package com.chenxin.fastdfs.controller;

import com.chenxin.fastdfs.client.FastDfsClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DeleteController {

    private static Logger logger = LoggerFactory.getLogger(UploadController.class);

    @RequestMapping("/delete/{id}")
    public String delete() throws Exception {
        int group1 = FastDfsClient.deleteFile("group1", "M00/00/00/CgxpDWEKX8GABtP-AAApQ090-AE407.gif");
        //删除结果0表示成功，2表示不存在
        logger.info("删除结果是：{}", group1);
        return null;
    }
}
